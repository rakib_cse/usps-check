import logzero
import pandas as pd

from logzero import logger
from requests_html import HTMLSession

# Configure logzero
logzero.logfile("logs/temp.log")

# Configure Request Session
session = HTMLSession()

# Variables
address_results = []
input_filename = "resources/input.csv"
output_filename = "resources/output.csv"


for row in pd.read_csv(input_filename).iterrows():

    try:
        # Form data to be sent with post request
        form_data = {
            "companyName": row[1]["Company"],
            "address1": row[1]["Street"],
            "address2": "",
            "city": row[1]["City"],
            "state": row[1]["St"],
            "urbanCode": "",
            "zip": row[1]["ZIPCode"]
        }

        logger.debug(
            f"Checking address for company: {row[1]['Company']}, Row: {row[0]+2}")

        # Check if address is valid. Retry 3 times if data is not found
        for _ in range(3):

            resp = session.post(
                "https://tools.usps.com/tools/app/ziplookup/zipByAddress", data=form_data)

            # USPS always returns the result status whether the address is valid or not
            # In this case, 200 response code is always expected. If any error occurs then retry
            if resp.status_code == 200:

                if resp.json().get("resultStatus") == "SUCCESS":
                    address_results.append({
                        **row[1],
                        "isValid": True
                    })
                else:
                    address_results.append({
                        **row[1],
                        "isValid": False
                    })

                # Break loop if data is found
                break

    except Exception as err:
        logger.error(
            f"Invalid data found. Company: {row[1]['Company']} Row: {row[0]+2}")

# Write result data into the csv file
pd.DataFrame(
    address_results,
    columns=["Company", "Street", "City", "St", "ZIPCode", "isValid"]
).to_csv(output_filename, index=False)

logger.debug(f"Data exported to file: {output_filename}")

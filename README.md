# usps-check

The script checks Address status based on the values provided in a CSV file

### Instructions to the script:

1. Pull the repository from GitHub into a directory
2. Create virtual enviroment with the command `virtualenv venv` and activate it.
3. Install the dependencies listed on the **requirements.txt** file by
   the command `pip install -r requirements.txt`
4. Create **resources** and put the input file into that folder. Create **logs** folder for the logs too.
5. Run the python script from the **usps-check** folder with the
   command `python src/main.py`

### Resources to run the script

Input file link: https://docs.google.com/spreadsheets/d/1H1a9eBamflt3w-4BPEk1kJYc4VgsDBWlDjkS0hV5tAY/edit?usp=sharing
